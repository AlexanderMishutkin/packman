from walls import TYPES


def calc(current, nb, fnb):
    left, top, right, bottom = nb
    lt, rt, rb, lb = fnb
    empties = ["*"," ","0","@",]
    playing = ["*", " ", "@"]
    walls = ["#", "-", "+"]
    if (left in walls) and (right in walls) and (top in playing):
        return TYPES["hor_bottom"]
    if (left in walls) and (right in walls) and (bottom in playing):
        return TYPES["hor_top"]
    if (top in walls) and (bottom in walls) and (left in playing):
        return TYPES["vert_right"]
    if (top in walls) and (bottom in walls) and (right in playing):
        return TYPES["vert_left"]
    if (top in playing) and (right in playing):
        return TYPES["."]["lb"]
    if (top in playing) and (left in playing):
        return TYPES["."]["rb"]
    if (bottom in playing) and (left in playing):
        return TYPES["."]["rt"]
    if (bottom in playing) and (right in playing):
        return TYPES["."]["lt"]
    if (top not in playing) and (right not in playing) and (bottom not in playing) and (left not in playing):
        if (lt in playing):
            return TYPES["L"]["rb"]
        if (lb in playing):
            return TYPES["L"]["rt"]
        if (rb in playing):
            return TYPES["L"]["lt"]
        if (rt in playing):
            return TYPES["L"]["lb"]
    else:
        return TYPES["L"]["lt"]

