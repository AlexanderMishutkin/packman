import pygame
import graphics_constants

class scorebar:
    def __init__(self, screen, score, cords):
        self.screen = screen
        self.cords = cords
        self.score = score

    def draw_score(self):
        font = pygame.font.SysFont("Comic Sans", 40)
        about = font.render(str(self.score),False, graphics_constants.GREEN)
        self.screen.blit(about, self.cords)
