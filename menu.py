from random import randint

import pygame
import sys
from button import Button
from graphics_constants import BLACK, RED, GREEN, SIZE, PURPLE, WHITE, YELLOW, ABOUT
from graphics_constants import GAME_STATE

class Menu:
    def __init__(self):
        d24 = SIZE[1] // 24
        dx4 = SIZE[0] // 4
        self.font = pygame.font.SysFont("Comic Sans", 40)
        self.gameover = False
        self.game_state = GAME_STATE["MENU"]
        self.responce = 0
        self.choose_level_button = Button((dx4,6 * d24,2 * dx4,4 * d24), PURPLE, self.choose_level, text = "CHOOSE LEVEL", hover_color = YELLOW, clicked_color = GREEN, hover_font_color = BLACK)
        self.level_1_button = Button((dx4, 6 * d24, 2 * dx4, 4 * d24), PURPLE, self.choosed_lvl1, text="LEVEL 1", hover_color=YELLOW, clicked_color = GREEN, hover_font_color = BLACK)
        self.about_button = Button((dx4,12 * d24,2 * dx4,4 * d24), PURPLE, self.about, text = "ABOUT", hover_color = YELLOW, clicked_color = GREEN, hover_font_color = BLACK)
        self.exit_button = Button((dx4,18 * d24,2 * dx4,4 * d24), PURPLE, self.exit, text = "EXIT", hover_color = YELLOW, clicked_color = GREEN, hover_font_color = BLACK)

    def choosed_lvl1(self):
        self.responce = 1

    def about(self):
        self.game_state = GAME_STATE["ABOUT"]
        self.arr = ABOUT
        for i in range(1, len(self.arr) - 1):
            if randint(0, 2):
                self.arr[i], self.arr[i + 1] = self.arr[i + 1], self.arr[i]
        for i in range(1, len(self.arr) - 1):
            if randint(0, 2):
                self.arr[i], self.arr[i + 1] = self.arr[i + 1], self.arr[i]

    def back(self):
        self.game_state = GAME_STATE["MENU"]

    def choose_level(self):
        self.game_state = GAME_STATE["CHOOSE LEVEL"]

    def exit(self):
        sys.exit()

    def main_loop(self, screen, events):
        self.process_events(events)
        self.process_drawing(screen)
        return self.responce

    def process_events(self, events):
        for event in events:
            if event.type == pygame.QUIT:
                self.responce = -1
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.game_state = GAME_STATE["MENU"]
            if self.game_state == GAME_STATE["MENU"]:
                self.choose_level_button.check_event(event)
                self.about_button.check_event(event)
                self.exit_button.check_event(event)
            if self.game_state == GAME_STATE["CHOOSE LEVEL"]:
                self.level_1_button.check_event(event)

    def process_drawing(self, screen):
        if (self.game_state == GAME_STATE["MENU"]):
            self.choose_level_button.update(screen)
            self.about_button.update(screen)
            self.exit_button.update(screen)
        elif (self.game_state == GAME_STATE["CHOOSE LEVEL"]):
            self.level_1_button.update(screen)
        elif (self.game_state == GAME_STATE["ABOUT"]):
            ind = 1
            for text in self.arr:
                about = self.font.render(text,False, GREEN)
                screen.blit(about, (100, 100 * ind))
                ind += 1
        else:
            pass
