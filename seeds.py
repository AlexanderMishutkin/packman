#Olek_Olegovich
import pygame
import entity
from graphics_constants import SIZE_CELL, WHITE, BLACK
from math import pi

class seed(entity.Entity):
    def __init__(self, center, type, alive):
        self.center = center
        self.type = type # Super (True) or not (False), Boolean
        self.alive = alive # Boolean
        if(self.type == False):
            self.rect = (center[0] - SIZE_CELL // 5, center[1] - SIZE_CELL // 5, SIZE_CELL // 5 * 2, SIZE_CELL // 5 * 2)
        else:
            self.rect = (center[0] - SIZE_CELL // 2, center[1] - SIZE_CELL // 2, SIZE_CELL // 2 * 2, SIZE_CELL // 2 * 2)
        super().__init__(self.rect)

    def draw(self, screen):
        if self.alive == True:
            center = self.center
            if(self.type == False):
                pygame.draw.circle(screen, WHITE, center, SIZE_CELL // 5)
            else:
                pygame.draw.circle(screen, WHITE, center, SIZE_CELL // 2)

    def points(self):
        if(self.type == False):
            pass
