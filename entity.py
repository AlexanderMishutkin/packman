import pygame

class Entity:
    def __init__(self,rect):
        self.rect = rect

    def draw(self, screen):
        pass

    def collides(self, other):
        r1 = pygame.Rect(self.rect)
        r2 = pygame.Rect(other.rect)
        return r1.colliderect(r2)

    def collides_after_move(self, other, direction):
        e = Entity((self.rect[0] + direction[0], self.rect[1] + direction[1], self.rect[2], self.rect[3]))
        return e.collides(other)
