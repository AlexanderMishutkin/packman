import pygame
import map
import walls
import gates
import seeds
from wall_type_callc import calc
from graphics_constants import SIZE, SIZE_CELL, WHITE, BLACK

class grid:
    def __init__(self, mp, position = (0, 0), cell_size = SIZE_CELL):
        self.map = mp
        self.size = cell_size
        self.pos = position
        self.centers = []
        for i in range(0, self.map.size[1]):
            for j in range(0, self.map.size[0]):
                self.centers.append(((self.size + 1)//2 + self.pos[0] + self.size * j, (self.size + 1)//2 + self.pos[1] + self.size * i))

    def draw_template(self, screen):
        for c in self.centers:
            pygame.draw.circle(screen, (100,100,0), c, self.size // 2 - 2, 2)

    def find_neighbores(self, ind):
        left = " "
        top = " "
        right = " "
        bottom  = " "
        if ind % self.map.size[0] > 0:
            left = self.map.map[ind - 1]
        if ind % self.map.size[0] < self.map.size[0] - 1:
            right = self.map.map[ind + 1]
        if ind >= self.map.size[0]:
            top = self.map.map[ind - self.map.size[0]]
        if ind < self.map.size[0] * self.map.size[1] - self.map.size[0]:
            bottom = self.map.map[ind + self.map.size[0]]
        return left, top, right, bottom

    def find_fnb(self, ind):
        lt = "0"
        rt = "0"
        rb = "0"
        lb = "0"
        if ind % self.map.size[0] > 0:
            if ind >= self.map.size[0]:
                lt = self.map.map[ind - self.map.size[0] - 1]
            if ind < self.map.size[0] * self.map.size[1] - self.map.size[0]:
                lb = self.map.map[ind + self.map.size[0] - 1]
        if ind % self.map.size[0] < self.map.size[0] - 1:
            if ind >= self.map.size[0]:
                rt = self.map.map[ind - self.map.size[0] + 1]
            if ind < self.map.size[0] * self.map.size[1] - self.map.size[0]:
                rb = self.map.map[ind + self.map.size[0] + 1]
        return lt, rt, rb, lb

    def init_walls(self):
        ind = 0
        self.walls = []
        for i in self.map.map:
            if i == "+" or i == "#":
                self.walls.append(walls.wall(self.centers[ind], calc(i, self.find_neighbores(ind), self.find_fnb(ind)), self.size))
            ind += 1
        self.walls.append(walls.wall([-SIZE_CELL,SIZE[1]//2 + SIZE_CELL], 1, SIZE_CELL))
        self.walls.append(walls.wall([-SIZE_CELL,SIZE[1]//2 - SIZE_CELL - SIZE_CELL - 2 * SIZE_CELL // 6], 1, SIZE_CELL))
        self.walls.append(walls.wall([SIZE[0],SIZE[1]//2 + SIZE_CELL], 0, SIZE_CELL))
        self.walls.append(walls.wall([SIZE[0],SIZE[1]//2 - SIZE_CELL - SIZE_CELL - 2 * SIZE_CELL // 6], 0, SIZE_CELL))

    def init_gates(self):
        ind = 0
        self.gates = []
        for i in self.map.map:
            if i == "-":
                self.gates.append(
                    gates.gate(self.centers[ind], self.size))
            ind += 1

    def init_seeds(self):
        ind = 0
        self.seeds = []
        for i in self.map.map:
            if i == "*":
                self.seeds.append(seeds.seed(self.centers[ind], False, True))
            elif i == "@":
                self.seeds.append(seeds.seed(self.centers[ind], True, True))
            ind += 1

    def draw_walls(self, screen):
        for i in self.walls:
            i.draw(screen)

    def draw_gates(self, screen):
        for i in self.gates:
            i.draw(screen)

    def draw_seeds(self, screen):
        for i in self.seeds:
            i.draw(screen)

    def draw_all(self, screen):
        self.draw_walls(screen)
        self.draw_gates(screen)
        self.draw_seeds(screen)


def create_grid(pos = (0,0), mp = map.classical_map()):
    g = grid(mp, pos)
    g.init_walls()
    g.init_gates()
    g.init_seeds()
    return g


def calc_center(center, grid):
    center = (int(center[0]), int(center[1]))
    pos = -1
    arr = grid.centers
    for i in range(0, len(arr)):
        if arr[i] == center:
            pos = i
    return pos

def example_app():
    pygame.init()
    screen = pygame.display.set_mode(SIZE)
    g = create_grid()
    g.init_gates()
    g.init_walls()
    while(1):
        screen.fill(BLACK)
        g.draw_all(screen)
        pygame.display.flip()


