import pygame

from entity import Entity

class Ghost(Entity):
    def __init__(self, rect, id_ghost):
        self.id_ghost = id_ghost
        self.rect = rect
        self.center = [self.rect[0] + self.rect[2] / 2, self.rect[1] + self.rect[3] / 2]
        self.eaten = False
        self.timeout = 0
        self.timer = 0

    def movement(self, x, y):
        self.rect[0] += x
        self.rect[1] += y
        self.center = [self.rect[0] + self.rect[2] / 2, self.rect[1] + self.rect[3] / 2]

    def draw(self, screen):
        if(self.eaten):
            self.sprites = pygame.image.load("Sprites/ghosts/tile004.png")
        else:
            if(self.id_ghost == 1):
                self.sprites = pygame.image.load("Sprites/ghosts/tile000.png")
            elif(self.id_ghost == 2):
                self.sprites = pygame.image.load("Sprites/ghosts/tile001.png")
            elif(self.id_ghost == 3):
                self.sprites = pygame.image.load("Sprites/ghosts/tile002.png")
            elif(self.id_ghost == 4):
                self.sprites = pygame.image.load("Sprites/ghosts/tile003.png")

        self.sprites = pygame.transform.scale(self.sprites, (self.rect[2], self.rect[3]),)
        self.rect_sprites=self.sprites.get_rect()

        screen.blit(self.sprites, self.rect)
