import pygame

from entity import Entity


class Pacman(Entity):
    def __init__(self, rect):
        self.cycle = 0
        self.shift = [0, -1]
        self.rect = rect
        self.center = [self.rect[0] + self.rect[2] / 2, self.rect[1] + self.rect[3] / 2]

    def movement(self, x, y):
        self.shift = x, y
        self.rect[0] += x
        self.rect[1] += y
        self.center = [self.rect[0] + self.rect[2] / 2, self.rect[1] + self.rect[3] / 2]

    def draw(self, screen):
        t = (0, 3, 6, 9)
        if self.cycle > t[3]:
            self.cycle = 0
        if self.shift[0] > 0:
            if self.cycle == t[0]:
                self.sprites = pygame.image.load("Sprites/packmen/tile000.png")
            elif self.cycle == t[1]:
                self.sprites = pygame.image.load("Sprites/packmen/tile001.png")
            elif self.cycle == t[2]:
                self.sprites = pygame.image.load("Sprites/packmen/tile002.png")
            elif self.cycle == t[3]:
                self.sprites = pygame.image.load("Sprites/packmen/tile001.png")
        elif (self.shift[1] < 0):
            if (self.cycle == t[0]):
                self.sprites = pygame.image.load("Sprites/packmen/tile000.png")
            elif (self.cycle == t[1]):
                self.sprites = pygame.image.load("Sprites/packmen/tile007.png")
            elif (self.cycle == t[2]):
                self.sprites = pygame.image.load("Sprites/packmen/tile008.png")
            elif (self.cycle == t[3]):
                self.sprites = pygame.image.load("Sprites/packmen/tile007.png")

        elif (self.shift[1] > 0):
            if (self.cycle == t[0]):
                self.sprites = pygame.image.load("Sprites/packmen/tile000.png")
            elif (self.cycle == t[1]):
                self.sprites = pygame.image.load("Sprites/packmen/tile003.png")
            elif (self.cycle == t[2]):
                self.sprites = pygame.image.load("Sprites/packmen/tile004.png")
            elif (self.cycle == t[3]):
                self.sprites = pygame.image.load("Sprites/packmen/tile003.png")

        elif (self.shift[0] < 0):
            if (self.cycle == t[0]):
                self.sprites = pygame.image.load("Sprites/packmen/tile000.png")
            elif (self.cycle == t[1]):
                self.sprites = pygame.image.load("Sprites/packmen/tile005.png")
            elif (self.cycle == t[2]):
                self.sprites = pygame.image.load("Sprites/packmen/tile006.png")
            elif (self.cycle == t[3]):
                self.sprites = pygame.image.load("Sprites/packmen/tile005.png")

        self.sprites = pygame.transform.scale(self.sprites, (self.rect[2], self.rect[3]), )

        screen.blit(self.sprites, self.rect)

        self.cycle += 1
