import pygame
import entity

WHITE = (255, 255, 255)

class gate(entity.Entity):
    def __init__(self, center, size):
        self.center = center
        self.rect = (0,0,0,0)
        self.size = size
        self.main_rect = self.rect
        super().__init__(self.rect)

    def draw(self, screen):
        center = self.center
        size = self.size
        d12 =  size // 12
        d6 = size // 6
        d2 =  size // 2
        d3 = size // 3
        rect = (center[0] - d2, center[1] + d2 - d3 + d12, size, d3  - d6 - d12)
        pygame.draw.rect(screen, WHITE, rect)