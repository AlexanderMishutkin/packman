

import random
import map
import floid_cash
class AI:
    def __init__(self, mp, ghost, pacman, chars_in_string, mode = 0):
        self.mp_obj = map.classical_map()
        self.mp = self.mp_obj.map
        self.pacman = pacman
        self.ghost = ghost
        self.cis = chars_in_string
        if mode == 0:
            self.pacman = self.mp_obj.size[0] * self.mp_obj.size[1] - self.pacman
            if (self.pacman >= self.mp_obj.size[0] * self.mp_obj.size[1] - 1):
                self.pacman = 1
            if (self.pacman < 1):
                self.pacman = 1
            while(self.mp[self.pacman] in ['+','#']):
                self.pacman += 1
                self.pacman %= (self.mp_obj.size[0] * self.mp_obj.size[1] - 1)

    def kost(self, a):
        g = self.ghost
        if (a == 0):
            if (self.mp[g - 1] != '#' and self.mp[g - 1] != '+'):
                return True
            return False
        elif (a == 1):
            if (self.mp[g + 1] != '#' and self.mp[g + 1] != '+'):
                return True
            return False
        elif (a == 2):
            if (self.mp[g - self.cis] != '#' and self.mp[g - self.cis] != '+'):
                return True
            return False
        else:
            if (self.mp[g + self.cis] != '#' and self.mp[g + self.cis] != '+' and self.mp[g + self.cis] != '-'):
                return True
            return False

    def movement(self):
        if floid_cash.a[self.ghost - 1][self.pacman] <= floid_cash.a[self.ghost + 1][self.pacman] and floid_cash.a[self.ghost - 1][self.pacman] <= floid_cash.a[self.ghost + 30][self.pacman] and floid_cash.a[self.ghost - 1][self.pacman] <= floid_cash.a[self.ghost - 30][self.pacman]:
            #if (self.pacman < self.ghost): # left up
            if (self.kost(0) == True):
                return (-1, 0)
        elif floid_cash.a[self.ghost + 1][self.pacman] <= floid_cash.a[self.ghost - 1][self.pacman] and floid_cash.a[self.ghost + 1][self.pacman] <= floid_cash.a[self.ghost + 30][self.pacman] and floid_cash.a[self.ghost + 1][self.pacman] <= floid_cash.a[self.ghost - 30][self.pacman]:
            if (self.kost(1) == True):
                return (1, 0)
        elif floid_cash.a[self.ghost + 30][self.pacman] <= floid_cash.a[self.ghost - 1][self.pacman] and floid_cash.a[self.ghost + 30][self.pacman] <= floid_cash.a[self.ghost + 1][self.pacman] and floid_cash.a[self.ghost + 30][self.pacman] <= floid_cash.a[self.ghost - 30][self.pacman]:
            if (self.kost(3) == True):
                return (0, 1)
        elif floid_cash.a[self.ghost - 30][self.pacman] <= floid_cash.a[self.ghost - 1][self.pacman] and floid_cash.a[self.ghost - 30][self.pacman] <= floid_cash.a[self.ghost + 1][self.pacman] and floid_cash.a[self.ghost - 30][self.pacman] <= floid_cash.a[self.ghost + 30][self.pacman]:
            if (self.kost(3) == True):
                return (0, -1)
        else: # right down
            x = random.randint(0, 2)
            if(x == 0):
                if (self.kost(1) == True):
                    return (1, 0)
                if (self.kost(3) == True):
                    return (0, 1)
            else:
                if (self.kost(3) == True):
                    return (0, 1)
                if (self.kost(1) == True):
                    return (1, 0)
        r = random.randint(0, 4)
        while (self.kost(r) == False):
            r = random.randint(0, 4)
        if (r == 0):
            return (-1, 0)
        elif (r == 1):
            return (1, 0)
        elif (r == 2):
            return (0, -1)
        else:
            return (0, 1)