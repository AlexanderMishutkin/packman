import pygame
import entity
from math import pi
from graphics_constants import *


TYPES = {
    "vert_left" : 1,
    "vert_right" : 0,
    "hor_top" : 2,
    "hor_bottom" : -1,
    "." : {
        "lt" : 3,
        "lb" : 4,
        "rt" : 5,
        "rb" : 6,
    },
    "L" : {
        "lt" : 7,
        "lb" : 8,
        "rt" : 9,
        "rb" : 10,
    }
}

class wall(entity.Entity):
    def __init__(self, center, type, size, double = False):
        self.doble = double
        self.center = center
        self.type = type
        self.rect = (0,0,0,0)
        self.size = size
        if type == TYPES["vert_left"]:
            self.rect = (center[0] - size//2, center[1] - size//2, size // 3, size)
        if type == TYPES["vert_right"]:
            self.rect = (center[0] + size//2 - size // 3, center[1] - size//2, size // 3, size)
        if type == TYPES["hor_top"]:
            self.rect = (center[0] - size//2, center[1] - size//2, size, size // 3)
        if type == TYPES["hor_bottom"]:
            self.rect = (center[0] - size // 2, center[1] + size // 2 - size // 3, size, size // 3)
        if type == TYPES["."]["lb"] or type == TYPES["L"]["lb"]:
            self.rect = (center[0] - size // 2, center[1] + size // 2 - size // 3, size // 3, size // 3)
        if type == TYPES["."]["lt"] or type == TYPES["L"]["lt"]:
            self.rect = (center[0] - size // 2, center[1] - size // 2, size // 3, size // 3)
        if type == TYPES["."]["rb"] or type == TYPES["L"]["rb"]:
            self.rect = (center[0] + size // 2 - size // 3, center[1] + size // 2 - size // 3, size // 3, size // 3)
        if type == TYPES["."]["rt"] or type == TYPES["L"]["rt"]:
            self.rect = (center[0] + size // 2 - size // 3, center[1] - size // 2, size // 3, size // 3)

        super().__init__(self.rect)

    def draw(self, screen):
        center = self.center
        size = self.size
        type = self.type
        d12 =  size // 12
        d6 = size // 6
        d2 =  size // 2
        d3 = size // 3
        if not self.doble:
            if type == TYPES["vert_left"]:
                rect = (center[0] - size//2 + d12, center[1] - size//2, size // 3 - d12, size)
                pygame.draw.rect(screen, PURPLE, rect)
            if type == TYPES["vert_right"]:
                rect = (center[0] + size//2 - size // 3, center[1] - size//2, size // 3 - d12, size)
                pygame.draw.rect(screen, PURPLE, rect)
            if type == TYPES["hor_top"]:
                rect = (center[0] - d2, center[1] - d2 + d12, size, d3 - d12)
                pygame.draw.rect(screen, PURPLE, rect)
            if type == TYPES["hor_bottom"]:
                rect = (center[0] - d2, center[1] + d2 - d3, size, d3  - d12)
                pygame.draw.rect(screen, PURPLE, rect)
            if type == TYPES["."]["lb"]: # or type == TYPES["L"]["lb"]
                rect = (center[0] - d2 - d3 -1, center[1] + d2 - d3, d3 * 2, d3 * 2)
                pygame.draw.arc(screen, PURPLE, rect, 0, pi / 2, d3 - d12)
            if type == TYPES["."]["lt"]:
                rect = (center[0] - d2 - d3, center[1] - d2 - d3 -1, d3 * 2, d3 * 2)
                pygame.draw.arc(screen, PURPLE, rect, -pi / 2, 0, d3 - d12)
            if type == TYPES["."]["rb"]: # or type == TYPES["L"]["rb"]:
                rect = (center[0] + d2 - d3, center[1] + d2 - d3, d3 * 2, d3 * 2)
                pygame.draw.arc(screen, PURPLE, rect, pi / 2, pi, d3 - d12)
            if type == TYPES["."]["rt"]:
                rect = (center[0] + d2 - d3, center[1] - d2 - d3, d3 * 2, d3 * 2)
                pygame.draw.arc(screen, PURPLE, rect, -pi, -pi / 2, d3 - d12)
            if type == TYPES["L"]["lt"]:
                rect = (center[0] - d2 + d12, center[1] - d2 + d12, size * 2 - d3 + d6, size * 2 - d3 + d6)
                pygame.draw.arc(screen, PURPLE, rect, pi/2, pi, d3 - d12)
            if type == TYPES["L"]["rt"]:
                rect = (center[0] - size - d2 + d12, center[1] - d2 + d12, size * 2 - d3 + d6, size * 2 - d3 + d6)
                pygame.draw.arc(screen, PURPLE, rect, 0, pi/2, d3 - d12)
            if type == TYPES["L"]["lb"]:
                rect = (center[0] - d2 + d12, center[1] - d2 * 3 + d12, size * 2 - d3 + d6, size * 2 - d3 + d6)
                pygame.draw.arc(screen, PURPLE, rect, -pi, -pi / 2, d3 - d12)

            if type == TYPES["L"]["rb"]:
                rect = (center[0] - size - d2 + d12, center[1] - d2 * 3 + d12, size * 2 - d3 + d6, size * 2 - d3 + d6)
                pygame.draw.arc(screen, PURPLE, rect, -pi / 2, 0, d3 - d12)
        else:
            pass