SIZE_CELL = 24
SIZE = (SIZE_CELL * 30, SIZE_CELL * 33)
WHITE = (250, 240, 190)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
PURPLE = (50, 20, 200)
YELLOW = (200, 200, 0)

FPS, DRAWING_FPS = SIZE_CELL * 11 , 18

SCARE_MOD_TICKS = 2000

FONT_SIZE = int(SIZE_CELL * 1.5)

ABOUT = ["authors:", "Alexander Mishutkin", "Oleg Manzhula", "Denis Milovidov", "Maxim Bessonov"]

GAME_STATE = {
    "MENU": 0,
    "GAME": 1,
    "SETTINGS": 2,
    "ABOUT": 3,
    "EXIT": 4,
    "CHOOSE LEVEL": 5
}
