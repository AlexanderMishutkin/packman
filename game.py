import pygame
import map
from graphics_constants import SIZE, SIZE_CELL, WHITE, BLACK, GAME_STATE, FPS, DRAWING_FPS
import engine
import levels_constants
#import AI
import menu
import time


pygame.init()
icon = pygame.image.load('Sprites/packmen/tile001.png')
icon = pygame.transform.scale(icon, (1000,1000))
pygame.display.set_icon(icon)
screen = pygame.display.set_mode(SIZE)

maps = ['',]
maps.append(map.classical_map())

AIs = ['',]
#AIs.append(si.classical_map())

pos = ['',]
pos.append(levels_constants.levels["classic"])

menu_responce = 0
mn = menu.Menu()

while(menu_responce != -1):
    while(menu_responce == 0):
        screen.fill(BLACK)
        events = pygame.event.get()
        menu_responce = mn.main_loop(screen, events)
        pygame.display.flip()
    if (menu_responce != -1):
        engine_responce = 0
        eg = engine.Engine(maps[menu_responce], 'AIs[menu_responce]', pos[menu_responce]["pacman"], pos[menu_responce]["ghosts"] )
        last_draw = time.time()
        last_tick = time.time()
        while(engine_responce == 0):
            events = pygame.event.get()
            engine_responce = eg.step(screen, events)
            for e in events:
                if e.type == pygame.QUIT:
                    menu_responce = -1
                    engine_responce = -1
                if e.type == pygame.KEYDOWN:
                    if e.key == pygame.K_ESCAPE:
                        engine_responce = -1
                        menu_responce = 0
                        mn.game_state = GAME_STATE["MENU"]
                        mn.responce = 0
            t = time.time()
            if t - last_draw > (1 / (DRAWING_FPS)):
                screen.fill(BLACK)
                eg.draw(screen)
                pygame.display.flip()
                last_draw = t
            t = time.time()
            if t - last_tick < (1 / (FPS)):
                pygame.time.delay(int(((1 / (FPS)) - t + last_tick)*1000))
            last_tick = time.time()
            if engine_responce == 1:
                #you_win()
                #menu_responce = 2
                menu_responce = 0
                mn.responce = 0
            if engine_responce == -1:
                #you_loose()
                #menu_responce = 2
                menu_responce = 0
                mn.responce = 0
                mn.game_state = menu.GAME_STATE["MENU"]