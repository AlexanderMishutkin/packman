import pygame
import entity
import graphics
import pacman
import ghost
import map
import draw_score
import AI
from graphics_constants import SIZE_CELL, WHITE, BLACK, SIZE, PURPLE, SCARE_MOD_TICKS

class Engine:
    def __init__(self, mp, si, pacman_coards, ghosts_centers):
        self.map = mp
        self.grid = graphics.create_grid((0,8), mp)
        self.pcmn_center = self.grid.centers[pacman_coards]
        self.pacman_coards = [self.pcmn_center[0] - SIZE_CELL//2 - 2 * SIZE_CELL//3, self.pcmn_center[1] - 2 * SIZE_CELL//3 - SIZE_CELL//2, 2 * SIZE_CELL + 2 * SIZE_CELL//6 , 2 * SIZE_CELL+ 2 * SIZE_CELL//6]
        self.const_pacman_coards = [self.pcmn_center[0] - SIZE_CELL//2 - 2 * SIZE_CELL//3, self.pcmn_center[1] - 2 * SIZE_CELL//3 - SIZE_CELL//2, 2 * SIZE_CELL + 2 * SIZE_CELL//6 , 2 * SIZE_CELL+ 2 * SIZE_CELL//6]
        self.pacman = pacman.Pacman(self.pacman_coards)
        self.ghosts = []
        self.ghosts_centers = ghosts_centers
        self.ghosts_coards = ['','','','','','']
        for i in range(len(self.ghosts_centers)):
            self.ghosts_coards[i] = self.grid.centers[ghosts_centers[i]]
        for i in range(1, 5):
            self.ghosts.append(ghost.Ghost([self.ghosts_coards[i - 1][0] - SIZE_CELL//2 - 2 * SIZE_CELL//3, self.ghosts_coards[i - 1][1] - 2 * SIZE_CELL//3 - SIZE_CELL//2, 2 * SIZE_CELL + 2 * SIZE_CELL//6 , 2 * SIZE_CELL+ 2 * SIZE_CELL//6], i))
        self.si = si
        self.responce = 0
        self.score = 0
        self.timer = 0
        self.clocks = 0
        self.move = [(0, 0),(0, 0),(0, 0),(0, 0)]
        self.str_pacman = 15
        self.timer_kost = 0
        self.lives = 3
        self.sprites = pygame.image.load("Sprites/packmen/tile001.png")
        self.sprites = pygame.transform.scale(self.sprites, (20, 20))

    def move_pacman(self, events):
        loose = False
        ind = 0
        for g in self.ghosts:
            sg = [0,0,0,0]
            sg[0] = g.rect[0] + 5
            sg[1] = g.rect[1] + 5
            sg[2] = g.rect[2] - 10
            sg[3] = g.rect[3] - 10
            sg = ghost.Ghost(sg, 1)
            if (self.pacman.collides(sg)):
                if g.eaten:
                    self.score += 200
                    self.ghosts[ind] = ghost.Ghost([self.ghosts_coards[ind][0] - SIZE_CELL//2 - 2 * SIZE_CELL//3, self.ghosts_coards[ind][1] - 2 * SIZE_CELL//3 - SIZE_CELL//2, 2 * SIZE_CELL + 2 * SIZE_CELL//6 , 2 * SIZE_CELL+ 2 * SIZE_CELL//6], ind + 1)
                    self.ghosts[ind].eaten = True
                    self.move[ind] = (0,0)
                else:
                    loose = True
            ind += 1
        if loose:
            return loose
        moved = True
        dir = self.pacman.shift
        keys = pygame.key.get_pressed()
        if keys[97]:
            can_move = True
            for w in (self.grid.walls):
                if self.pacman.collides_after_move(w, (-1, 0)):
                    can_move = False
            if can_move:
                moved = False
                self.pacman.movement(-1, 0)
        if keys[115]:
            can_move = True
            for w in (self.grid.walls):
                if self.pacman.collides_after_move(w, (0, 1)):
                    can_move = False
            if can_move:
                moved = False
                self.pacman.movement(0, 1)
        if keys[119]:
            can_move = True
            for w in (self.grid.walls):
                if self.pacman.collides_after_move(w, (0, -1)):
                    can_move = False
            if can_move:
                moved = False
                self.pacman.movement(0, -1)
        if keys[100]:
            can_move = True
            for w in (self.grid.walls):
                if self.pacman.collides_after_move(w, (1, 0)):
                    can_move = False
            if can_move:
                moved = False
                self.pacman.movement(1, 0)
        if moved:
            can_move = True
            for w in (self.grid.walls):
                if self.pacman.collides_after_move(w, dir):
                    can_move = False
            if can_move:
                self.pacman.movement(dir[0], dir[1])
        if self.pacman.rect[0] + self.pacman.rect[3] < 0:
            self.pacman.rect[0] += self.map.size[0] * SIZE_CELL + self.pacman.rect[3]
        if self.pacman.rect[0] > self.map.size[0] * SIZE_CELL:
            self.pacman.rect[0] -= self.map.size[0] * SIZE_CELL + self.pacman.rect[3]
        for s in (self.grid.seeds):
            if self.pacman.collides(s):
                if(s.alive == True):
                    if(s.type == False):
                        self.score += 10
                    else:
                        self.score += 50
                        self.timer = SCARE_MOD_TICKS
                    s.alive = False
        return False

    def step(self, screen, events):
        for g in self.ghosts:
            g.timeout -= 1
        self.clocks += 1
        self.clocks %= 4
        if(self.timer > 0):
            self.timer -= 1
        loose = self.move_pacman(events)
        if loose:
            pygame.time.delay(500)
            if self.lives <= 0:
                return -1;
            self.lives -= 1;
            self.ghosts = []
            for i in range(1, 5):
                self.ghosts.append(ghost.Ghost([self.ghosts_coards[i - 1][0] - SIZE_CELL//2 - 2 * SIZE_CELL//3, self.ghosts_coards[i - 1][1] - 2 * SIZE_CELL//3 - SIZE_CELL//2, 2 * SIZE_CELL + 2 * SIZE_CELL//6 , 2 * SIZE_CELL+ 2 * SIZE_CELL//6], i))
            self.pacman_coards = [self.pcmn_center[0] - SIZE_CELL//2 - 2 * SIZE_CELL//3, self.pcmn_center[1] - 2 * SIZE_CELL//3 - SIZE_CELL//2, 2 * SIZE_CELL + 2 * SIZE_CELL//6 , 2 * SIZE_CELL+ 2 * SIZE_CELL//6]
            self.pacman = pacman.Pacman(self.pacman_coards)
            self.move = [(0, 0),(0, 0),(0, 0),(0, 0)]
            self.draw(screen)
            pygame.time.delay(100)


        if(graphics.calc_center(self.pacman.center, self.grid) > 0):
            self.str_pacman = graphics.calc_center(self.pacman.center, self.grid)
        else:
            self.timer_kost -= 1
        id = 0
        if self.clocks != 4 and self.clocks != 2:
            for g in self.ghosts:
                if g.timeout <= 0:
                    g.eaten = bool(self.timer > 0)
                    if (graphics.calc_center(g.center, self.grid) > 0):
                        a = ''
                        if not g.eaten:
                            a = AI.AI(map, graphics.calc_center(g.center, self.grid), self.str_pacman, 30, id + 1)
                        else:
                            a = AI.AI(map, graphics.calc_center(g.center, self.grid), self.str_pacman, 30)
                        self.move[id] = a.movement()
                    g.movement(self.move[id][0], self.move[id][1])
                    id += 1
                else:
                    self.move[id] = (0,0)
        win = True
        for s in self.grid.seeds:
            win = win and (not s.alive)
        if win:
            return 1
        return self.responce

    def draw(self, screen):
        self.grid.draw_all(screen)
        self.pacman.draw(screen)
        sb = draw_score.scorebar(screen, self.score, (600,0))
        sb.draw_score()
        for i in range(0, self.lives):
            screen.blit(self.sprites, (i*20,0,20,20))
        for i in self.ghosts:
            i.draw(screen)
